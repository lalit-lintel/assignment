from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor, protocol, stdio
import sys
import re
from twisted.protocols import basic


class EchoTerminal(basic.LineReceiver):
    from os import linesep as delimiter

    def __init__(self, client_self):
        self.client_self = client_self

    def connectionMade(self):
        self.transport.write('>>>\n')

    def lineReceived(self, line):
        print "You entered", line
        self.client_self.transport.write(line)


class ClientProtocol(basic.LineReceiver):
    def __init__(self, factory):
        stdio.StandardIO(EchoTerminal(self))

    def dataReceived(self, data):
        print data,


class ClientFactory(protocol.ClientFactory):
    def buildProtocol(self, addr):
        return ClientProtocol(self)


reactor.connectTCP("localhost", 8000, ClientFactory())
reactor.run()
