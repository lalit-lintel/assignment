from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver
from twisted.internet import reactor
from twisted.enterprise import adbapi
import re

conn=adbapi.ConnectionPool("MySQLdb",db="employee",user="root",passwd="root")




class CrudProtocol(LineReceiver):
	def __init__(self,factory):
		self.name=""
		self.salary=""

	def connectionMade(self):
		self.sendLine("Welcome To Employee Register Application\n\nEnter to continue")

	"""
		Decorator Methods
	"""
	
	def deco(fun):
		def wra(obj,line):
			if(re.match("^[ ]*(insert) +[a-zA-Z]{1,20} +[0-9]{1,9}[ ]*$",line) or 
				re.match("^[ ]*(update)[ ]+[0-9]{1,9}[ ]+[a-zA-Z]{1,20}[ ]+[0-9]{1,9}[ ]*$",line) or
				re.match("^[ ]*(delete)[ ]+[0-9]{1,9}[ ]*$",line)):
				return fun(obj,line)			
		return wra
	
	

	def lineReceived(self,line):
		self.printChoice()
		p=re.compile("^ *(show|insert|update|delete|quit).*$")
		if p.match(line):
			fun={"show":self.show,"insert":self.insert,"update":self.update,"delete":self.delete,"quit":self.quit}[line.split()[0]]
			fun(line)
		
	def show(self,line):
		d=conn.runQuery("select * from emp")
		d.addCallback(self.printAll)

	"""
		Database Operation Methods
	"""

	@deco
	def insert(self,line):
		arg=line.split()
		query="insert into emp(name,salary) values('%s',%s)" %(arg[1],arg[2])
		conn.runQuery(query)
	
	@deco
	def update(self,line):
		arg=line.split()
		query="update emp set name = '%s' ,salary = %s where id = %s" %(arg[2],arg[3],arg[1])
		conn.runQuery(query)
	
	@deco
	def delete(self,line):
		arg=line.split()
		query="delete from emp where id = %s" %(arg[1])
		conn.runQuery(query)
	
	def quit(self,line):
		self.sendLine("\033c")
		self.transport.loseConnection()
	

	def printAll(self,results):
		self.sendLine("\033c")
		self.printChoice()
		self.sendLine("\n\nAllRescord\n")
		self.sendLine("%-5s%-20s%-10s\n" %("ID","Name","Salary"))
		for i in results:
			self.sendLine("%-5s%-20s%-10s" %(i[0],i[1],i[2]))
		self.sendLine("")

	def printChoice(self):
		self.sendLine("\033c")
		self.sendLine("Show All Record Syntax : show")
		self.sendLine("Insert Record syntax : insert name salary")
		self.sendLine("Delete Record Syntax : delete id")
		self.sendLine("Update Record Syntax : update id name salary")
		self.sendLine("Exit Syntax : quit\n")

class CrudFactory(Factory):
	def buildProtocol(self,addr):
		return CrudProtocol(self)

reactor.listenTCP(8000,CrudFactory())
reactor.run()
