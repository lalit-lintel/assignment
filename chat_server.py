from twisted.internet import reactor
from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver
import re


class Group(object):
    def __init__(self):
        self.groups = {}

    def createGroup(self, name, admin, pcol):
        if name in self.groups:
            pcol.sendLine("Groups name Is Already Exist")
            return False
        else:
            self.groups[name] = {"members": [pcol], "admins": [admin]}
            return True

    def retJoinGroups(self, pcol):
        return [k for k, v in self.groups.items() if pcol in v["members"]]

    def addMember(self, name, pcol):
        if name not in self.groups:
            pcol.sendLine("Group is not Exist")
        else:
            self.groups[name]["members"].append(pcol)

    def getMembers(self, name):
        return [i for i in self.groups[name]["members"]]

    def removeMember(self, name, pcol):
        if pcol in self.groups[name]["members"]:
            self.groups[name]["members"].remove(pcol)

    def getGroups(self):
        return [i for i in self.groups]


class MyFactory(Factory):
    def __init__(self):
        self.users = {}
        self.g = Group()

    def buildProtocol(self, addr):
        return MyProtocol(self)


class MyProtocol(LineReceiver):
    menu = """
create <group>   : create new group
groups           : view existing group
members <group>  : view member group
chat <group>     : start chat to group
close            : leave group
"""

    def __init__(self, factory):
        self.factory = factory
        self.name = ""
        self.status = ""
        self.group = ""

    def connectionMade(self):
        self.sendLine("Welcome To Chat Application\n\nEnter Your Name")

    def connectionLost(self, reason):
        del self.factory.users[self.name]
        if self.group != "":
            self.factory.g.removeMember(self.group, self)

    def promt(self):
        if self.status != "":
            if self.group == "":
                self.transport.write("Command >> ")
            else:
                self.transport.write("%s >> " % (self.group))

    def blankCheck(fun):
        def wra(self, line):
            print line
            if line != "":
                return fun(self, line)
            else:
                self.promt()

        return wra

    @blankCheck
    def dataReceived(self, line):
        if self.status == "":
            self.register(line)
        elif self.group != "":
            self.chat(line)
        elif re.match("^(create|groups|members|chat)$", line.split()[0]):
            fun = {"create": self.createGroup, "chat": self.chatGroup, "groups": self.viewGroups,
                   "members": self.viewMembers}[line.split()[0]]
            fun(line)
        self.promt()

    def validateCreate(fun):
        def wra(self, cmd):
            if re.match("^[ ]*(create)[ ]+[A-Za-z0-9]+[ ]*$", cmd):
                return fun(self, cmd.split()[1])
            else:
                self.sendLine("Syntax Error")

        return wra

    @validateCreate
    def createGroup(self, name):
        if self.factory.g.createGroup(name, self.name, self):
            self.group = name

    def validateChatGroup(fun):
        def wra(self, cmd):
            if re.match("^[ ]*(chat)[ ]+[A-Za-z0-9]+[ ]*$", cmd):
                return fun(self, cmd.split()[1])

        return wra

    @validateChatGroup
    def chatGroup(self, name):
        if name in self.factory.g.getGroups():
            self.group = name
            self.factory.g.addMember(name, self)

    def register(self, name):
        if name in self.factory.users:
            self.sendLine("Name Already Taken Give Another Name")
        else:
            self.factory.users[name] = self
            self.name = name
            self.status = "done"
            self.sendLine("\033cWelcome " + name)
            self.transport.write(self.menu)

    def viewGroups(self, line):
        groups = self.factory.g.getGroups()
        for i in groups:
            self.sendLine(i)

    def validateViewMembers(fun):
        def wra(self, cmd):
            if re.match("^[ ]*(members)[ ]+[A-Za-z0-9]+[ ]*$", cmd):
                return fun(self, cmd.split()[1])

        return wra

    @validateViewMembers
    def viewMembers(self, name):
        members = self.factory.g.getMembers(name)
        for i in members:
            self.sendLine(i.name)

    def checkQuit(fun):
        def wra(self, line):
            if line == "close":
                self.factory.g.removeMember(self.group, self)
                self.group = ""
            else:
                return fun(self, line)

        return wra

    @checkQuit
    def chat(self, line):
        for i in self.factory.g.getMembers(self.group):
            if i != self:
                i.sendLine("\r<%s> %s" % (self.name, line))
                i.transport.write("%s >> " % (self.group))


reactor.listenTCP(8000, MyFactory())
reactor.run()
